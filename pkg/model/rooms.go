package model

import (
	"time"

	"github.com/go-playground/validator/v10"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	qmodel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

const (
	defaultQuestionCount   = 10
	defaultPlayingDuration = time.Minute * 5
)

type RoomStatus int

// statuses of the room
const (
	Waiting RoomStatus = iota
	Starting
	Playing
	Ending
)

var modelValidator *validator.Validate = bingameutils.NewValidator()

type Room struct {
	ID                     string                  `validate:"bingame_room_id"`
	Name                   string                  `validate:"max=50" redis:"name"`
	HostID                 string                  `validate:"required,uuid5" redis:"hostid"`
	MaxPlayers             uint32                  `validate:"required,min=1,max=40" redis:"maxplayers"`
	QuestionCount          uint32                  `validate:"required,min=1,max=1000" redis:"questioncount"`
	Status                 RoomStatus              `redis:"status"`
	LastTimeStarted        *time.Time              `redis:"laststarted"` // is nil if the room never started
	QuestionSetID          string                  `validate:"omitempty,uuid4" redis:"questionset"`
	FinishedAnsweringCount uint32                  `validate:"ltefield=MaxPlayers" redis:"finishedanswering"`
	PlayingDuration        time.Duration           `validate:"min=30s" redis:"playingdurationseconds"`
	Level                  qmodel.QuestionSetLevel `redis:"level"`
	Shuffle                bool                    `redis:"shuffle"`
}

func New(hostId string, roomName string, maxPlayers uint32, playingDuration *time.Duration) (*Room, error) {
	newRID := bingameutils.GenerateRoomID(4)

	r := &Room{
		ID:                     newRID,
		Name:                   roomName,
		HostID:                 hostId,
		MaxPlayers:             maxPlayers,
		QuestionCount:          defaultQuestionCount,
		Status:                 Waiting,
		LastTimeStarted:        nil,
		QuestionSetID:          "",
		FinishedAnsweringCount: 0,
		Level:                  qmodel.Easy,
		Shuffle:                false,
	}

	if playingDuration != nil {
		r.PlayingDuration = *playingDuration
	} else {
		r.PlayingDuration = defaultPlayingDuration
	}

	err := modelValidator.Struct(r)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func (r *Room) Validate() error {
	return modelValidator.Struct(r)
}
