package model

import (
	"errors"
	"strconv"
	"time"

	"github.com/oklog/ulid/v2"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
)

var ErrUnknownEventType error = errors.New("unknown event type")

type EventType int

const (
	/* events */

	RoomStarting EventType = iota
	RoomPlaying
	RoomEnding
	RoomEdited
	UserHostChanged
	UserJoined
	UserLeft
	UserBanned
	UserUnbanned
	UserLostConnection

	/* errors */

	ErrorStarting
	ErrorEnding
)

func (e EventType) String() string {
	switch e {
	case RoomEdited:
		return "RoomEdited"
	case RoomStarting:
		return "RoomStarting"
	case RoomPlaying:
		return "RoomPlaying"
	case RoomEnding:
		return "RoomEnding"
	case UserHostChanged:
		return "UserHostChanged"
	case UserJoined:
		return "UserJoined"
	case UserLeft:
		return "UserLeft"
	case UserBanned:
		return "UserBanned"
	case UserUnbanned:
		return "UserUnbanned"
	case UserLostConnection:
		return "UserLostConnection"
	case ErrorStarting:
		return "ErrorStarting"
	case ErrorEnding:
		return "ErrorEnding"
	default:
		return "Unknown:" + strconv.Itoa(int(e))
	}
}

// Event interface definition
type Event interface {
	GetID() string
	GetTimestamp() time.Time
	GetType() EventType
	GetDescription() string
	GetRoomID() string
}

// BaseEvent struct with embedded fields
type BaseEvent struct {
	ID          ulid.ULID
	Timestamp   time.Time
	Type        EventType
	RoomID      string
	Description string
}

// roomEvent struct embedding baseEvent
type GenericEvent struct {
	BaseEvent
}

// userEvent struct embedding baseEvent and adding UserId
type UserEvent struct {
	BaseEvent
	UserId string
}

type RoomStartingEvent struct {
	BaseEvent
	QuestionSetId string
	StartsIn      time.Duration
}

type RoomEditedEvent struct {
	BaseEvent
	RoomName        string
	MaxPlayers      uint32
	PlayingDuration time.Duration
}

/*
  Implement the Event interface for the base type
*/

func (e *BaseEvent) GetID() string {
	return e.ID.String()
}

func (e *BaseEvent) GetTimestamp() time.Time {
	return e.Timestamp
}

func (e *BaseEvent) GetType() EventType {
	return e.Type
}

func (e *BaseEvent) GetDescription() string {
	return e.Description
}

func (e *BaseEvent) GetRoomID() string {
	return e.RoomID
}

/*
  User event functions
*/

func (e *UserEvent) GetUserId() string {
	return e.UserId
}

/*
  Start match event function
*/

func (e *RoomStartingEvent) GetQuestionSetId() string {
	return e.QuestionSetId
}

func (e *RoomStartingEvent) GetStartsIn() time.Duration {
	return e.StartsIn
}

/*
  Room edited event functions
*/

func (e *RoomEditedEvent) GetRoomName() string {
	return e.RoomName
}

func (e *RoomEditedEvent) GetMaxPlayers() uint32 {
	return e.MaxPlayers
}

func (e *RoomEditedEvent) GetPlayingDuration() time.Duration {
	return e.PlayingDuration
}

/*
  Implement the Event interface methods for GenericEvent
*/

// Factory function for creating a new GenericEvent
func NewGenericEvent(eventType EventType, roomId string, description string) Event {
	return &GenericEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   time.Now(),
			Type:        eventType,
			Description: description,
			RoomID:      roomId,
		},
	}
}

// Factory function for creating a new GenericEvent.
// This function is useful for synchronizing the event with other functions.
func NewGenericEventWithTimestamp(timestamp time.Time, eventType EventType, roomId string, description string) Event {
	return &GenericEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   timestamp,
			Type:        eventType,
			RoomID:      roomId,
			Description: description,
		},
	}
}

// Factory function for creating a new UserEvent
func NewUserEvent(eventType EventType, roomId string, userId string, description string) Event {
	return &UserEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   time.Now(),
			Type:        eventType,
			Description: description,
			RoomID:      roomId,
		},
		UserId: userId,
	}
}

func NewRoomStartingEvent(roomId string, description string, questionSetId string) Event {
	return &RoomStartingEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   time.Now(),
			Type:        RoomStarting,
			Description: description,
			RoomID:      roomId,
		},
		QuestionSetId: questionSetId,
		StartsIn:      constants.DefaultStartsIn,
	}
}

// Factory function to create a new RoomEditedEvent
func NewRoomEditedEvent(roomId string, roomName string, maxPlayers uint32, questionCount uint32, playingDuration time.Duration) Event {
	return &RoomEditedEvent{
		BaseEvent: BaseEvent{
			ID:          ulid.Make(),
			Timestamp:   time.Now(),
			Type:        RoomEdited,
			Description: "Room edited",
			RoomID:      roomId,
		},
		RoomName:        roomName,
		MaxPlayers:      maxPlayers,
		PlayingDuration: playingDuration,
	}
}

// NewEvent creates a specific Event based on the BaseEvent Type
func NewEvent(baseEvent BaseEvent) (Event, error) {
	switch baseEvent.Type {
	case RoomEdited:
		return &RoomEditedEvent{BaseEvent: baseEvent}, nil
	case RoomStarting:
		return &RoomStartingEvent{BaseEvent: baseEvent}, nil
	case RoomPlaying, RoomEnding, ErrorStarting:
		return &GenericEvent{BaseEvent: baseEvent}, nil
	case UserJoined, UserLeft, UserBanned, UserUnbanned, UserHostChanged, UserLostConnection:
		return &UserEvent{BaseEvent: baseEvent}, nil
	default:
		return nil, ErrUnknownEventType
	}
}
