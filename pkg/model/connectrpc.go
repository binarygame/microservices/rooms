package model

import (
	"github.com/oklog/ulid/v2"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	roomsv1 "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1"
)

// Map to convert a connectrpc room status to a model.RoomStatus
var ProtoStatusMap = map[roomsv1.RoomStatus]RoomStatus{
	roomsv1.RoomStatus_ROOM_STATUS_WAITING:  Waiting,
	roomsv1.RoomStatus_ROOM_STATUS_STARTING: Starting,
	roomsv1.RoomStatus_ROOM_STATUS_PLAYING:  Playing,
	roomsv1.RoomStatus_ROOM_STATUS_ENDING:   Ending,
}

// ConvertProtoStatus converts the room status type from the ConnectRPC format back to the
// internal model format.
//
// Returns ErrRoomStatusInvalid if the status is not recognized.
func ConvertProtoStatus(protoStatus roomsv1.RoomStatus) (RoomStatus, error) {
	internalStatus, exists := ProtoStatusMap[protoStatus]
	if !exists {
		return Waiting, errdefs.ErrRoomStatusInvalid
	}
	return internalStatus, nil
}

type eventCreator func(*roomsv1.Event, BaseEvent) (Event, error)

var eventTypeHandlers = map[roomsv1.EventType]eventCreator{
	roomsv1.EventType_EVENT_TYPE_ROOM_STARTING:             createRoomStartingEvent,
	roomsv1.EventType_EVENT_TYPE_ROOM_PLAYING:              createGenericEvent(RoomPlaying),
	roomsv1.EventType_EVENT_TYPE_ROOM_ENDING:               createGenericEvent(RoomEnding),
	roomsv1.EventType_EVENT_TYPE_ROOM_USER_HOST_CHANGED:    createUserEvent(UserHostChanged),
	roomsv1.EventType_EVENT_TYPE_ROOM_USER_JOINED:          createUserEvent(UserJoined),
	roomsv1.EventType_EVENT_TYPE_ROOM_USER_LEFT:            createUserEvent(UserLeft),
	roomsv1.EventType_EVENT_TYPE_ROOM_USER_BANNED:          createUserEvent(UserBanned),
	roomsv1.EventType_EVENT_TYPE_ROOM_USER_UNBANNED:        createUserEvent(UserUnbanned),
	roomsv1.EventType_EVENT_TYPE_ROOM_USER_LOST_CONNECTION: createUserEvent(UserLostConnection),
	roomsv1.EventType_EVENT_TYPE_ROOM_ERROR_STARTING:       createGenericEvent(ErrorStarting),
}

func createRoomStartingEvent(protoEvent *roomsv1.Event, baseEvent BaseEvent) (Event, error) {
	roomStartingEvent := protoEvent.GetRoomStartingEvent()
	if roomStartingEvent == nil {
		return nil, errdefs.ErrEventTypeInvalid
	}
	baseEvent.Type = RoomStarting
	return &RoomStartingEvent{
		BaseEvent:     baseEvent,
		QuestionSetId: roomStartingEvent.GetQuestionSetId(),
		StartsIn:      roomStartingEvent.GetStartsIn().AsDuration(),
	}, nil
}

func createGenericEvent(eventType EventType) eventCreator {
	return func(protoEvent *roomsv1.Event, baseEvent BaseEvent) (Event, error) {
		baseEvent.Type = eventType
		return &GenericEvent{BaseEvent: baseEvent}, nil
	}
}

func createUserEvent(eventType EventType) eventCreator {
	return func(protoEvent *roomsv1.Event, baseEvent BaseEvent) (Event, error) {
		userEvent := protoEvent.GetUserEvent()
		if userEvent == nil {
			return nil, errdefs.ErrEventTypeInvalid
		}
		baseEvent.Type = eventType
		return &UserEvent{
			BaseEvent: baseEvent,
			UserId:    userEvent.UserId,
		}, nil
	}
}

// ConvertProtoEvent converts the room event type from the ConnectRPC format back to the
// internal event format.
//
// Returns ErrEventTypeInvalid if the event is malformed.
//
// Returns ErrUlidInvalid if the event id can't be parsed as an ULID.
func ConvertProtoEvent(protoEvent *roomsv1.Event) (Event, error) {
	// Convert id
	id, err := ulid.ParseStrict(protoEvent.EventId)
	if err != nil {
		return nil, errdefs.ErrUlidInvalid
	}

	// Base event initialization
	baseEvent := BaseEvent{
		Type:        UserBanned, // Default type to ensure it's set
		ID:          id,
		Timestamp:   protoEvent.EventTime.AsTime(),
		RoomID:      protoEvent.GetRoomId(),
		Description: protoEvent.GetDescription(),
	}

	// Use the map to find the appropriate event creator function
	if handler, exists := eventTypeHandlers[protoEvent.EventType]; exists {
		return handler(protoEvent, baseEvent)
	}
	return nil, errdefs.ErrEventTypeInvalid
}
