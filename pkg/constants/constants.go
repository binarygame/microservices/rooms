package constants

import "time"

// app configs
const (
	ExpireDuration  = 2 * time.Hour
	DefaultStartsIn = 5 * time.Second

	JoinRoomPingInterval = 15 * time.Second
)

// telemetry configs
const (
	NamespaceKey    = "gitlab.com/binarygame/microservices/rooms"
	RoomsCreatedKey = "rooms_created"
	RoomsPlayingKey = "rooms_playing"

	MatchesPlayedKey    = "matches_played"
	MatchDurationAvgKey = "match_duration_seconds_avg"
	TotalPlayedTimeKey  = "total_played_time_minutes"

	UsersRegisteredKey     = "users_registered"
	UsersRegisteredPingKey = "users_registered_ping"
	UsersBannedKey         = "users_banned"
	UsersUnbannedKey       = "users_unbanned"
)
