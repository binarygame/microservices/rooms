package errdefs

import "errors"

var (
	/*
	   Usecase errors
	*/
	ErrInvalidData         = errors.New("data provided is invalid")
	ErrRoomIsFull          = errors.New("room is already at its maximum capacity of player")
	ErrBindUserRoomFailed  = errors.New("an error occurred while associating user to room")
	ErrForbbidenAccess     = errors.New("unauthorized access")
	ErrUserAuthInvalid     = errors.New("user authentication failed")
	ErrInternalServer      = errors.New("internal server error")
	ErrUserAlreadyFinished = errors.New("user has already finished answering")
	ErrUserBannedFromRoom  = errors.New("user tried to join room but is banned from it")
	ErrRoomAlreadyStarted  = errors.New("Cannot modify room data because the room has already started")

	/*
	   Database-specific errors
	*/
	ErrDatabaseFailed          = errors.New("an error occurred while connecting with the database")
	ErrRoomDoesNotExist        = errors.New("tried to access/modify room data but room did not exist")
	ErrRoomContainsInvalidData = errors.New("a field in the room has invalid data saved on it")

	/*
	   ConnectRPC type conversion errors
	*/
	ErrEventTypeInvalid         = errors.New("an invalid event type was received")
	ErrUlidInvalid              = errors.New("the event ULID is invalid")
	ErrRoomStatusInvalid        = errors.New("received an invalid room status number")
	ErrRoomDifficultyOutOfRange = errors.New("the question's difficulty value is outside the allowed range")
)
