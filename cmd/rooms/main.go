package main

import (
	"context"
	"errors"
	"log/slog"
	"net/http"

	"github.com/joho/godotenv"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1/guessesv1connect"
	questionsv1connect "gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1/questionsv1connect"
	"gitlab.com/binarygame/microservices/rooms/internal/repository"
	"gitlab.com/binarygame/microservices/rooms/internal/server/connectrpc/service"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
)

func main() {
	err := godotenv.Load()
	logger := bingameutils.GetLogger()
	if err != nil {
		logger.Info("Error loading .env file")
	}

	serviceHost := bingameutils.GetServiceHost()
	servicePort := bingameutils.GetServicePort()

	slog.SetDefault(logger)

	// Set up OpenTelemetry.
	ts, otelShutdown, err := telemetry.New(context.Background(), constants.NamespaceKey)
	if err != nil {
		logger.Error("failed to set up OpenTelemetry SDK", "error", err.Error())
	}
	defer func() {
		err = errors.Join(err, otelShutdown(context.Background()))
	}()

	questionsClient := questionsv1connect.NewQuestionsServiceClient(
		http.DefaultClient,
		"http://"+bingameutils.GetQuestionsServiceAddress(),
	)

	guessesClient := guessesv1connect.NewGuessesServiceClient(
		http.DefaultClient,
		"http://"+bingameutils.GetGuessesServiceAddress(),
	)

	repository := repository.New(bingameutils.GetValkeyAddress())
	service.Start(serviceHost, servicePort, repository, logger, questionsClient, guessesClient, ts)
}
