package service

import (
	"context"
	"errors"
	"strings"
	"time"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	roomsv1 "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"

	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (rs *RoomsServer) GetRoom(ctx context.Context, r *connect.Request[roomsv1.GetRoomRequest]) (*connect.Response[roomsv1.GetRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	room, err := rs.usecase.Get(ctx, r.Msg.GetRoomId())
	if err != nil {
		var code connect.Code
		switch err {
		case errdefs.ErrDatabaseFailed:
			code = connect.CodeInternal
		case errdefs.ErrRoomDoesNotExist:
			code = connect.CodeNotFound
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	// Convert room status to connectrpc format
	var roomStatus roomsv1.RoomStatus
	switch room.Status {
	case model.Waiting:
		roomStatus = roomsv1.RoomStatus_ROOM_STATUS_WAITING
	case model.Starting:
		roomStatus = roomsv1.RoomStatus_ROOM_STATUS_STARTING
	case model.Playing:
		roomStatus = roomsv1.RoomStatus_ROOM_STATUS_PLAYING
	case model.Ending:
		roomStatus = roomsv1.RoomStatus_ROOM_STATUS_ENDING
	default:
		roomStatus = roomsv1.RoomStatus_ROOM_STATUS_UNSPECIFIED
	}

	return &connect.Response[roomsv1.GetRoomResponse]{
		Msg: &roomsv1.GetRoomResponse{
			Name:          room.Name,
			HostId:        room.HostID,
			MaxPlayers:    int32(room.MaxPlayers),
			QuestionCount: int32(room.QuestionCount),
			RoomStatus:    roomStatus,
		},
	}, nil
}

func (rs *RoomsServer) CheckIfRoomExists(ctx context.Context, r *connect.Request[roomsv1.CheckIfRoomExistsRequest]) (*connect.Response[roomsv1.CheckIfRoomExistsResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	exists, err := rs.usecase.CheckIfRoomExists(ctx, r.Msg.RoomId)
	if err != nil {
		var code connect.Code
		switch err {
		case errdefs.ErrDatabaseFailed:
			code = connect.CodeInternal
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return &connect.Response[roomsv1.CheckIfRoomExistsResponse]{
		Msg: &roomsv1.CheckIfRoomExistsResponse{
			Exists: exists,
		},
	}, nil
}

func (rs *RoomsServer) GetUsersInRoom(ctx context.Context, r *connect.Request[roomsv1.GetUsersInRoomRequest]) (*connect.Response[roomsv1.GetUsersInRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	users, err := rs.usecase.GetUsers(ctx, r.Msg.RoomId)
	if err != nil {
		var code connect.Code
		switch err {
		case errdefs.ErrDatabaseFailed:
			code = connect.CodeInternal
		case errdefs.ErrRoomDoesNotExist:
			code = connect.CodeNotFound
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return &connect.Response[roomsv1.GetUsersInRoomResponse]{
		Msg: &roomsv1.GetUsersInRoomResponse{
			UserIds: users,
		},
	}, nil
}

func (rs *RoomsServer) CreateRoom(ctx context.Context, r *connect.Request[roomsv1.CreateRoomRequest]) (*connect.Response[roomsv1.CreateRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())

	// Convert protobuf duration to go duration
	var duration *time.Duration
	if r.Msg.MatchDuration != nil {
		d := r.Msg.GetMatchDuration().AsDuration()
		duration = &d
	}

	data, err := rs.usecase.Create(ctx, r.Msg.HostId, r.Msg.Name, r.Msg.MaxPlayers, duration)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		case errors.Is(err, errdefs.ErrBindUserRoomFailed):
			code = connect.CodeInternal
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return connect.NewResponse(&roomsv1.CreateRoomResponse{
		Data: &roomsv1.CreateRoomData{RoomId: data.ID},
	}), nil
}

func (rs *RoomsServer) EditRoom(ctx context.Context, r *connect.Request[roomsv1.EditRoomRequest]) (*connect.Response[roomsv1.EditRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())

	// Convert protobuf duration to go duration
	var duration *time.Duration
	if r.Msg.MatchDuration != nil {
		d := r.Msg.GetMatchDuration().AsDuration()
		duration = &d
	}

	var level questionsModel.QuestionSetLevel
	switch r.Msg.Difficulty {
	case roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_EASY:
		level = questionsModel.Easy
	case roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_MEDIUM:
		level = questionsModel.Medium
	case roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_HARD:
		level = questionsModel.Hard
	// case roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_RANDOM:
	// 	level = questionsModel.Random
	default:
		return nil, connect.NewError(connect.CodeInvalidArgument, errdefs.ErrRoomDifficultyOutOfRange)
	}

	err := rs.usecase.Edit(ctx, r.Msg.RoomId, r.Msg.Name, r.Msg.MaxPlayers, duration, level, r.Msg.Shuffle)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		case errors.Is(err, errdefs.ErrRoomDoesNotExist):
			code = connect.CodeNotFound
		case errors.Is(err, errdefs.ErrRoomAlreadyStarted):
			code = connect.CodeFailedPrecondition
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return connect.NewResponse(&roomsv1.EditRoomResponse{}), nil
}

func (rs *RoomsServer) RegisterUserInRoom(ctx context.Context, r *connect.Request[roomsv1.RegisterUserInRoomRequest]) (*connect.Response[roomsv1.RegisterUserInRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())

	expiresAt, wasUserInRoomBefore, err := rs.usecase.RegisterUser(ctx, r.Msg.UserId, r.Msg.RoomId)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		case errors.Is(err, errdefs.ErrRoomIsFull):
			code = connect.CodeResourceExhausted
		case errors.Is(err, errdefs.ErrRoomDoesNotExist):
			code = connect.CodeNotFound
		case errors.Is(err, errdefs.ErrUserBannedFromRoom):
			code = connect.CodePermissionDenied
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return connect.NewResponse(&roomsv1.RegisterUserInRoomResponse{
		ExpiresAt:       timestamppb.New(*expiresAt),
		WasInRoomBefore: wasUserInRoomBefore,
	}), nil
}

func (rs *RoomsServer) RemoveUserFromRoom(ctx context.Context, r *connect.Request[roomsv1.RemoveUserFromRoomRequest]) (*connect.Response[roomsv1.RemoveUserFromRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	err := rs.usecase.RemoveUser(ctx, r.Msg.UserId, r.Msg.RoomId)
	if err != nil {
		var code connect.Code

		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		}
		return nil, connect.NewError(code, err)
	}

	return connect.NewResponse(&roomsv1.RemoveUserFromRoomResponse{}), nil
}

func (rs *RoomsServer) GetQuestionCount(ctx context.Context, r *connect.Request[roomsv1.GetQuestionCountRequest]) (*connect.Response[roomsv1.GetQuestionCountResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())

	if len(strings.TrimSpace(r.Msg.RoomId)) == 0 {
		return nil, connect.NewError(connect.CodeInvalidArgument, errdefs.ErrInvalidData)
	}

	count, err := rs.usecase.GetQuestionCount(ctx, r.Msg.RoomId)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrRoomDoesNotExist):
			code = connect.CodeNotFound
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return connect.NewResponse(&roomsv1.GetQuestionCountResponse{
		Data: &roomsv1.QuestionCountData{QuestionCount: int32(count)},
	}), nil
}

func (rs *RoomsServer) StartMatch(ctx context.Context, r *connect.Request[roomsv1.StartMatchRequest]) (*connect.Response[roomsv1.StartMatchResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	err := rs.usecase.StartMatch(ctx, r.Msg.RoomId)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		case errors.Is(err, errdefs.ErrForbbidenAccess):
			code = connect.CodePermissionDenied
		case errors.Is(err, errdefs.ErrInternalServer):
			code = connect.CodeInternal
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return &connect.Response[roomsv1.StartMatchResponse]{}, nil
}

func (rs *RoomsServer) UserFinishedAnswering(ctx context.Context, r *connect.Request[roomsv1.UserFinishedAnsweringRequest]) (*connect.Response[roomsv1.UserFinishedAnsweringResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	err := rs.usecase.HandleUserFinishedAnswering(ctx, r.Msg.RoomId, r.Msg.UserId)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		case errors.Is(err, errdefs.ErrUserAlreadyFinished):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrInternalServer):
			code = connect.CodeInternal
		case errors.Is(err, errdefs.ErrRoomDoesNotExist):
			code = connect.CodeNotFound
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return &connect.Response[roomsv1.UserFinishedAnsweringResponse]{}, nil
}
