package service

import (
	"context"
	"errors"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	roomsv1 "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1"
)

func (rs *RoomsServer) BanUserFromRoom(ctx context.Context, r *connect.Request[roomsv1.BanUserFromRoomRequest]) (*connect.Response[roomsv1.BanUserFromRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	err := rs.usecase.BanUser(ctx, r.Msg.UserId, r.Msg.RoomId)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		case errors.Is(err, errdefs.ErrForbbidenAccess):
			code = connect.CodePermissionDenied
		case errors.Is(err, errdefs.ErrRoomDoesNotExist):
			code = connect.CodeNotFound
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return connect.NewResponse(&roomsv1.BanUserFromRoomResponse{}), nil
}

func (rs *RoomsServer) UnbanUserFromRoom(ctx context.Context, r *connect.Request[roomsv1.UnbanUserFromRoomRequest]) (*connect.Response[roomsv1.UnbanUserFromRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	err := rs.usecase.UnbanUser(ctx, r.Msg.UserId, r.Msg.RoomId)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		case errors.Is(err, errdefs.ErrForbbidenAccess):
			code = connect.CodePermissionDenied
		case errors.Is(err, errdefs.ErrRoomDoesNotExist):
			code = connect.CodeNotFound
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return connect.NewResponse(&roomsv1.UnbanUserFromRoomResponse{}), nil
}

func (rs *RoomsServer) GetBannedUsersFromRoom(ctx context.Context, r *connect.Request[roomsv1.GetBannedUsersFromRoomRequest]) (*connect.Response[roomsv1.GetBannedUsersFromRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	users, err := rs.usecase.GetBannedUsersFromRoom(ctx, r.Msg.RoomId)
	if err != nil {
		var code connect.Code
		switch {
		case errors.Is(err, errdefs.ErrInvalidData):
			code = connect.CodeInvalidArgument
		case errors.Is(err, errdefs.ErrDatabaseFailed):
			code = connect.CodeInternal
		default:
			code = connect.CodeUnknown
		}
		return nil, connect.NewError(code, err)
	}

	return connect.NewResponse(&roomsv1.GetBannedUsersFromRoomResponse{
		UserIds: users,
	}), nil
}
