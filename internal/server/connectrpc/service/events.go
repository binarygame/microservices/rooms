package service

import (
	"context"
	"encoding/json"

	"connectrpc.com/connect"
	roomsv1 "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (rs *RoomsServer) SubscribeAllRoomEvents(ctx context.Context, r *connect.Request[roomsv1.SubscribeAllRoomEventsRequest], s *connect.ServerStream[roomsv1.SubscribeAllRoomEventsResponse]) error {
	// Subscribe to room events
	rs.logger.Info("Subscribing to all room events")
	ch, err := rs.usecase.SubscribeToAll(ctx)
	if err != nil {
		rs.logger.Error("Failed to subscribe to all room events", "error", err)
		return connect.NewError(connect.CodeInternal, err)
	}

	for {
		select {
		case <-ctx.Done():
			rs.logger.Info("Context done, terminating subscription to all rooms")
			return nil
		case event := <-ch:
			var userId string
			protoEvent := rs.convertEvent(event)

			if userEvent, ok := protoEvent.Event.(*roomsv1.Event_UserEvent); ok {
				userId = userEvent.UserEvent.GetUserId()
			}

			rs.logger.Debug("Sending event",
				"roomId", event.GetRoomID(),
				"timestamp", event.GetTimestamp().String(),
				"description", event.GetDescription(),
				"eventType", protoEvent.EventType.String(),
				"userId", userId,
			)

			// Send the event via the stream
			err = s.Send(&roomsv1.SubscribeAllRoomEventsResponse{
				Data: protoEvent,
			})
			if err != nil {
				rs.logger.Error("Failed to send event via stream", "error", err)
				return connect.NewError(connect.CodeInternal, err)
			}
		}
	}
}

func (rs *RoomsServer) SubscribeRoomEvents(ctx context.Context, r *connect.Request[roomsv1.SubscribeRoomEventsRequest], s *connect.ServerStream[roomsv1.SubscribeRoomEventsResponse]) error {
	// Subscribe to room events
	rs.logger.Info("Subscribing to room events", "roomId", r.Msg.GetRoomId())
	ch, err := rs.usecase.Subscribe(ctx, r.Msg.GetRoomId())
	if err != nil {
		rs.logger.Error("Failed to subscribe to room events", "error", err)
		return connect.NewError(connect.CodeInternal, err)
	}

	for {
		select {
		case <-ctx.Done():
			rs.logger.Info("Context done, terminating subscription", "roomId", r.Msg.GetRoomId())
			return nil
		case event := <-ch:
			var userId string
			protoEvent := rs.convertEvent(event)

			if userEvent, ok := protoEvent.Event.(*roomsv1.Event_UserEvent); ok {
				userId = userEvent.UserEvent.GetUserId()
			}

			rs.logger.Debug("Sending event",
				"roomId", event.GetRoomID(),
				"timestamp", event.GetTimestamp().String(),
				"description", event.GetDescription(),
				"eventType", protoEvent.EventType.String(),
				"userId", userId,
			)

			// Send the event via the stream
			err = s.Send(&roomsv1.SubscribeRoomEventsResponse{
				Data: protoEvent,
			})
			if err != nil {
				rs.logger.Error("Failed to send event via stream", "error", err)
				return connect.NewError(connect.CodeInternal, err)
			}
		}
	}
}

func (rs *RoomsServer) convertEvent(event model.Event) *roomsv1.Event {
	protoEvent := &roomsv1.Event{
		EventId:   event.GetID(),
		EventTime: timestamppb.New(event.GetTimestamp()),
		RoomId:    event.GetRoomID(),
	}

	// Optional description
	if d := event.GetDescription(); d != "" {
		protoEvent.Description = &d
	}

	switch e := event.(type) {
	case *model.UserEvent:
		protoUserEvent := &roomsv1.UserEvent{UserId: e.GetUserId()}
		switch event.GetType() {
		case model.UserHostChanged:
			protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_USER_HOST_CHANGED
		case model.UserJoined:
			protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_USER_JOINED
		case model.UserLeft:
			protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_USER_LEFT
		case model.UserLostConnection:
			protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_USER_LOST_CONNECTION
		case model.UserBanned:
			protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_USER_BANNED
		}
		protoEvent.Event = &roomsv1.Event_UserEvent{UserEvent: protoUserEvent}
	case *model.RoomStartingEvent:
		protoRoomStartingEvent := &roomsv1.RoomStartingEvent{
			StartsIn:      durationpb.New(e.GetStartsIn()),
			QuestionSetId: e.GetQuestionSetId(),
		}
		protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_STARTING
		protoEvent.Event = &roomsv1.Event_RoomStartingEvent{RoomStartingEvent: protoRoomStartingEvent}
	case *model.RoomEditedEvent:
		protoRoomEditedEvent := &roomsv1.RoomEditedEvent{
			RoomName:        e.GetRoomName(),
			MaxPlayers:      e.GetMaxPlayers(),
			PlayingDuration: durationpb.New(e.GetPlayingDuration()),
		}
		protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_EDITED
		protoEvent.Event = &roomsv1.Event_RoomEditedEvent{RoomEditedEvent: protoRoomEditedEvent}
	case *model.GenericEvent:
		protoGenericEvent := &roomsv1.GenericEvent{}
		switch event.GetType() {
		case model.RoomPlaying:
			protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_PLAYING
		case model.RoomEnding:
			protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_ROOM_ENDING
		}
		protoEvent.Event = &roomsv1.Event_GenericEvent{GenericEvent: protoGenericEvent}
	default:
		j, err := json.Marshal(event)
		rs.logger.Error("Unspecified event type when converting to connectrpc type", "event", j, "json_error", err)
		protoEvent.EventType = roomsv1.EventType_EVENT_TYPE_UNSPECIFIED
	}

	return protoEvent
}
