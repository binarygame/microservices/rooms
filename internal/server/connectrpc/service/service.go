package service

import (
	"log/slog"
	"net/http"
	"strconv"

	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1/guessesv1connect"
	questionsv1connect "gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1/questionsv1connect"
	"gitlab.com/binarygame/microservices/rooms/internal/repository"
	"gitlab.com/binarygame/microservices/rooms/internal/usecase"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
	"gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1/roomsv1connect"

	"connectrpc.com/connect"
	"connectrpc.com/grpcreflect"
	"connectrpc.com/otelconnect"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

type RoomsServer struct {
	roomsv1connect.UnimplementedRoomsServiceHandler
	roomsv1connect.UnimplementedEventServiceHandler
	usecase usecase.Rooms
	logger  *slog.Logger
}

func newRoomsServer(usecase usecase.Rooms, logger *slog.Logger) *RoomsServer {
	return &RoomsServer{
		usecase: usecase,
		logger:  logger,
	}
}

func Start(ip string, port int, repository repository.RoomsRepository, logger *slog.Logger, questionsClient questionsv1connect.QuestionsServiceClient, guessesClient guessesv1connect.GuessesServiceClient, ts telemetry.TelemetryService) {
	addr := ip + ":" + strconv.Itoa(port)

	// TODO: handle match end and use MatchDurationAvg + TotalPlayedTimeKey

	ts.NewCounter(constants.RoomsCreatedKey, "Number of generated rooms", telemetry.IntCounterType)
	ts.NewCounterUpDown(constants.RoomsPlayingKey, "Number of rooms currently playing", telemetry.IntCounterType)
	ts.NewCounter(constants.MatchesPlayedKey, "Number of matches played", telemetry.IntCounterType)
	// TODO: fix panic
	// ts.NewHistogram(constants.MatchDurationAvgKey, "Average match duration in seconds", telemetry.FloatHistogramType)
	ts.NewCounter(constants.TotalPlayedTimeKey, "Total played time in minutes", telemetry.IntCounterType)
	ts.NewCounter(constants.UsersRegisteredKey, "Number of users registered", telemetry.IntCounterType)
	ts.NewCounter(constants.UsersBannedKey, "Number of users banned", telemetry.IntCounterType)

	usecase := usecase.New(repository, logger, questionsClient, guessesClient, ts)
	otelInterceptor, err := otelconnect.NewInterceptor(otelconnect.WithTrustRemote())
	if err != nil {
		logger.Error("An error occurred in otelconnect.NewInterceptor", "error", err)
	}

	server := newRoomsServer(usecase, logger)
	mux := http.NewServeMux()

	// Rooms handler
	roomsPath, roomsHandler := roomsv1connect.NewRoomsServiceHandler(
		server,
		connect.WithInterceptors(otelInterceptor),
	)
	eventsPath, eventsHandler := roomsv1connect.NewEventServiceHandler(
		server,
		connect.WithInterceptors(otelInterceptor),
	)

	// For GRPC reflection
	reflector := grpcreflect.NewStaticReflector(
		roomsv1connect.RoomsServiceName,
		roomsv1connect.EventServiceName,
	)

	mux.Handle(roomsPath, roomsHandler)
	mux.Handle(eventsPath, eventsHandler)
	mux.Handle(grpcreflect.NewHandlerV1(reflector))
	mux.Handle(grpcreflect.NewHandlerV1Alpha(reflector))

	logger.Info("Starting connect server for binarygame-rooms", "ip", ip, "port", port)

	err = http.ListenAndServe(
		addr,
		h2c.NewHandler(mux, &http2.Server{}),
	)
	if err != nil {
		logger.Error("An error occurred in http.ListenAndServe:", "error", err.Error())
	}
}
