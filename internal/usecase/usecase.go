package usecase

import (
	"context"
	"log/slog"
	"sync"
	"time"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1/guessesv1connect"
	"gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1/questionsv1connect"
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
	"gitlab.com/binarygame/microservices/rooms/internal/repository"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
)

type Rooms interface {
	Get(ctx context.Context, roomId string) (*model.Room, error)
	GetUsers(ctx context.Context, roomId string) ([]string, error)
	Create(ctx context.Context, hostID, roomName string, maxPlayers uint32, playingDuration *time.Duration) (*model.Room, error)
	Edit(ctx context.Context, roomId, roomName string, maxPlayers uint32, playingDuration *time.Duration, level questionsModel.QuestionSetLevel, shuffle bool) error
	RegisterUser(ctx context.Context, userId, roomId string) (expiresAt *time.Time, wasInRoomBefore bool, err error)
	RemoveUser(ctx context.Context, userId, roomId string) error
	BanUser(ctx context.Context, userId, roomId string) error
	UnbanUser(ctx context.Context, userId, roomId string) error
	GetBannedUsersFromRoom(ctx context.Context, roomId string) ([]string, error)
	GetQuestionCount(ctx context.Context, roomID string) (int, error)
	StartMatch(ctx context.Context, roomID string) error
	Subscribe(ctx context.Context, roomId string) (<-chan model.Event, error)
	SubscribeToAll(ctx context.Context) (<-chan model.Event, error)
	CheckIfRoomExists(ctx context.Context, id string) (bool, error)
	HandleUserFinishedAnswering(ctx context.Context, roomID string, userID string) error
}

type rooms struct {
	repository           repository.RoomsRepository
	questionsClient      questionsv1connect.QuestionsServiceClient
	guessesClient        guessesv1connect.GuessesServiceClient
	telemetry            telemetry.TelemetryService
	manager              *bingameutils.RoomManager[model.Event]
	logger               *slog.Logger
	userDisconnectTimers *roomUserMap
	endMatchTimers       sync.Map
}

func New(repo repository.RoomsRepository, logger *slog.Logger, questionsClient questionsv1connect.QuestionsServiceClient, guessesClient guessesv1connect.GuessesServiceClient, ts telemetry.TelemetryService) Rooms {
	rooms := &rooms{
		repository:      repo,
		logger:          logger,
		manager:         bingameutils.NewRoomManager[model.Event](),
		questionsClient: questionsClient,
		guessesClient:   guessesClient,
		telemetry:       ts,
	}

	rooms.userDisconnectTimers = newRoomUserMap(constants.JoinRoomPingInterval, rooms.UserDisconnected)
	rooms.startBackgroundTasks()
	return rooms
}
