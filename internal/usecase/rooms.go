package usecase

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"strconv"
	"strings"
	"time"

	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
)

// Get retrieves the room details for the specified roomId.
func (ru *rooms) Get(ctx context.Context, roomId string) (*model.Room, error) {
	logger := ru.logger.WithGroup("get").With(
		"room_id", roomId,
		"caller_id", bingameutils.GetUserIdFromCtx(ctx),
	)

	logger.LogAttrs(ctx, slog.LevelDebug, "Get called")

	span := ru.telemetry.Record(ctx, "Get",
		map[string]string{
			"roomId": roomId,
		})
	defer span.End()

	// validate input data
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return nil, errors.Join(errdefs.ErrInvalidData, err)
	}

	room, err := ru.repository.GetRoomById(ctx, roomId)
	if err != nil {
		if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
			logger.LogAttrs(ctx, slog.LevelInfo, "Room with provided id does not exist")
			return nil, err
		}
		logger.LogAttrs(ctx, slog.LevelError, "Failed to get room by ID", slog.String("error", err.Error()))
		return nil, errdefs.ErrDatabaseFailed
	}

	return room, nil
}

// CheckIfRoomExists checks if a room with the given roomId exists.
func (ru *rooms) CheckIfRoomExists(ctx context.Context, roomId string) (bool, error) {
	// validate input data
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return false, errors.Join(errdefs.ErrInvalidData, err)
	}

	exists, err := ru.repository.CheckIfRoomExists(ctx, roomId)
	if err != nil {
		return false, errdefs.ErrDatabaseFailed
	}
	return exists, nil
}

// Create creates a new room with the provided parameters.
func (ru *rooms) Create(ctx context.Context, hostId, roomName string, maxPlayers uint32, playingDuration *time.Duration) (*model.Room, error) {
	logger := ru.logger.WithGroup("create").With(
		"hostId", hostId,
		"roomName", roomName,
		"maxPlayers", maxPlayers,
		"callerId", bingameutils.GetUserIdFromCtx(ctx),
	)

	logger.LogAttrs(ctx, slog.LevelDebug, "Create called")

	span := ru.telemetry.Record(ctx, "Create",
		map[string]string{
			"hostId":     hostId,
			"roomName":   roomName,
			"maxPlayers": fmt.Sprint(maxPlayers),
		})
	defer span.End()

	// Fetch callerId and validate against hostId
	callerId := bingameutils.GetUserIdFromCtx(ctx)
	if (callerId != "" && callerId != hostId) || !bingameutils.IsValidUUID(hostId) {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: callerId mismatch or invalid hostId")
		return nil, errdefs.ErrInvalidData
	}

	// Create a new room model with the provided parameters
	room, err := model.New(hostId, roomName, maxPlayers, playingDuration)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to create room model", slog.String("error", err.Error()))
		return nil, errdefs.ErrInvalidData
	}

	err = ru.repository.InsertRoom(ctx, room)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to insert room into repository", slog.String("error", err.Error()))
		return nil, errdefs.ErrDatabaseFailed
	}

	err = ru.repository.InsertUserInRoom(ctx, hostId, room.ID, constants.JoinRoomPingInterval*2)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to insert user into room", slog.String("error", err.Error()))
		return nil, errdefs.ErrBindUserRoomFailed
	}

	// Add host to autodisconnect data structure
	ru.userDisconnectTimers.Set(room.ID, hostId)

	ru.telemetry.IncrementCounter(ctx, constants.RoomsCreatedKey, 1)

	return room, nil
}

// Edit updates the room with the provided parameters.
func (ru *rooms) Edit(ctx context.Context, roomId, roomName string, maxPlayers uint32, playingDuration *time.Duration, level questionsModel.QuestionSetLevel, shuffle bool) error {
	logger := ru.logger.WithGroup("edit").With(
		"roomId", roomId,
		"roomName", roomName,
		"maxPlayers", maxPlayers,
		"level", level,
		"shuffle", shuffle,
		"callerId", bingameutils.GetUserIdFromCtx(ctx),
	)

	logger.LogAttrs(ctx, slog.LevelDebug, "Edit called")

	span := ru.telemetry.Record(ctx, "Edit",
		map[string]string{
			"roomId":          roomId,
			"roomName":        roomName,
			"maxPlayers":      fmt.Sprint(maxPlayers),
			"playingDuration": fmt.Sprint(playingDuration),
			"level":           level.String(),
			"shuffle":         strconv.FormatBool(shuffle),
		})
	defer span.End()

	// validate input data
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return errors.Join(errdefs.ErrInvalidData, err)
	}

	room, err := ru.repository.GetRoomById(ctx, roomId)
	if err != nil {
		return errdefs.ErrDatabaseFailed
	}

	// Ensure the room is still waiting and hasn't started
	if room.Status != model.Waiting {
		logger.LogAttrs(ctx, slog.LevelError, "Room is currently playing, cannot edit")
		return errdefs.ErrRoomAlreadyStarted
	}

	// Update room properties with new values
	room.Name = roomName
	room.MaxPlayers = maxPlayers
	room.PlayingDuration = *playingDuration

	// Persist updated room
	err = ru.repository.UpdateRoom(ctx, room.ID, room.Name, room.HostID, room.MaxPlayers, room.QuestionCount, room.Status, room.LastTimeStarted, level, shuffle)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to update room", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	ru.repository.PublishEvent(ctx, model.NewRoomEditedEvent(room.ID, room.Name, room.MaxPlayers, room.QuestionCount, room.PlayingDuration))

	return nil
}

// GetQuestionCount retrieves the number of questions in a room.
func (ru *rooms) GetQuestionCount(ctx context.Context, roomId string) (int, error) {
	logger := ru.logger.WithGroup("get_question_count").With("roomId", roomId)

	logger.LogAttrs(ctx, slog.LevelDebug, "GetQuestionCount called")

	span := ru.telemetry.Record(ctx, "GetQuestionCount",
		map[string]string{"roomId": roomId})
	defer span.End()

	// validate input data
	if len(strings.TrimSpace(roomId)) == 0 {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: roomId is empty")
		return 0, errdefs.ErrInvalidData
	}

	count, err := ru.repository.GetQuestionCount(ctx, roomId)
	if err != nil {
		if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
			return 0, err
		}
		logger.LogAttrs(ctx, slog.LevelError, "Failed to get question count", slog.String("error", err.Error()))
		return 0, errdefs.ErrDatabaseFailed
	}

	return count, nil
}
