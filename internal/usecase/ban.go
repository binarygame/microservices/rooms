package usecase

import (
	"context"
	"errors"
	"log/slog"
	"strings"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
)

// BanUser bans a user from a room.
func (ru *rooms) BanUser(ctx context.Context, userId, roomId string) error {
	logger := ru.logger.WithGroup("ban_user").With(
		"userId", userId,
		"roomId", roomId,
		"callerId", bingameutils.GetUserIdFromCtx(ctx),
	)

	logger.LogAttrs(ctx, slog.LevelDebug, "BanUser called")

	span := ru.telemetry.Record(ctx, "BanUser",
		map[string]string{
			"userId": userId,
			"roomId": roomId,
		})
	defer span.End()

	// validate input data
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return errors.Join(errdefs.ErrInvalidData, err)
	}

	callerId := bingameutils.GetUserIdFromCtx(ctx)

	// TODO: move to bingameutils
	if len(strings.TrimSpace(userId)) == 0 || len(strings.TrimSpace(roomId)) == 0 {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: userId or roomId is empty")
		return errdefs.ErrInvalidData
	}

	roomObj, err := ru.repository.GetRoomById(ctx, roomId)
	if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
		return err
	}
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to get room by ID", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	// Ensure that only the host can ban users
	if callerId != "" && callerId != roomObj.HostID {
		logger.LogAttrs(ctx, slog.LevelError, "Forbidden access: caller is not the host")
		return errdefs.ErrForbbidenAccess
	}

	// Check if the user being banned is the host
	if userId == roomObj.HostID {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: the host cannot be banned")
		return errdefs.ErrInvalidData
	}

	// Ban the user from the room and handle errors
	err = ru.repository.BanUserFromRoom(ctx, userId, roomId)
	if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
		return err
	}
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to ban user from room", slog.Any("error", err))
		return errors.Join(errdefs.ErrDatabaseFailed, err)
	}

	// Check if the user is still in the room and remove them
	userInRoom, err := ru.repository.IsUserInRoom(ctx, roomId, userId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Could not check if user is in room", slog.Any("error", err))
		return errors.Join(errdefs.ErrDatabaseFailed, err)
	}

	if userInRoom {
		err = ru.repository.RemoveUserFromRoom(ctx, userId, roomId)
		if err != nil {
			logger.LogAttrs(ctx, slog.LevelError, "Failed to remove user from room", slog.String("error", err.Error()))
			return errors.Join(errdefs.ErrDatabaseFailed, err)
		}
		ru.repository.PublishEvent(ctx, model.NewUserEvent(model.UserLeft, roomId, userId, "User removed from room"))
	}

	ru.telemetry.IncrementCounter(ctx, constants.UsersBannedKey, 1)
	ru.repository.PublishEvent(ctx, model.NewUserEvent(model.UserBanned, roomId, userId, "User banned from room"))

	return nil
}

// UnbanUser lifts the ban on a user from a room.
func (ru *rooms) UnbanUser(ctx context.Context, userId, roomId string) error {
	logger := ru.logger.WithGroup("unban_user").With(
		"userId", userId,
		"roomId", roomId,
		"callerId", bingameutils.GetUserIdFromCtx(ctx),
	)

	logger.LogAttrs(ctx, slog.LevelDebug, "UnbanUser called")

	span := ru.telemetry.Record(ctx, "UnbanUser",
		map[string]string{
			"userId": userId,
			"roomId": roomId,
		})
	defer span.End()

	// Parse roomId and validate input data
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return errors.Join(errdefs.ErrInvalidData, err)
	}

	callerId := bingameutils.GetUserIdFromCtx(ctx)

	// Check for invalid userId or roomId
	if len(strings.TrimSpace(userId)) == 0 || len(strings.TrimSpace(roomId)) == 0 {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: userId or roomId is empty")
		return errdefs.ErrInvalidData
	}

	// Retrieve the room by ID and handle errors
	roomObj, err := ru.repository.GetRoomById(ctx, roomId)
	if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
		return err
	}
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to get room by ID", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	// Ensure that only the host can unban users
	if callerId != "" && callerId != roomObj.HostID {
		logger.LogAttrs(ctx, slog.LevelError, "Forbidden access: caller is not the host")
		return errdefs.ErrForbbidenAccess
	}

	// Check if the user being unbanned is the host
	if userId == roomObj.HostID {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: the host cannot be unbanned as it is never banned")
		return errdefs.ErrInvalidData
	}

	// Unban the user from the room and handle errors
	err = ru.repository.UnbanUserFromRoom(ctx, userId, roomId)
	if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
		return err
	}
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to unban user from room", slog.Any("error", err))
		return errors.Join(errdefs.ErrDatabaseFailed, err)
	}

	ru.telemetry.IncrementCounter(ctx, constants.UsersUnbannedKey, 1)
	ru.repository.PublishEvent(ctx, model.NewUserEvent(model.UserUnbanned, roomId, userId, "User unbanned from room"))

	return nil
}

// GetBannedUsersFromRoom retrieves a list of banned users from a room.
func (ru *rooms) GetBannedUsersFromRoom(ctx context.Context, roomId string) ([]string, error) {
	logger := ru.logger.WithGroup("get_banned_users_from_room").With(
		"roomId", roomId,
	)

	logger.LogAttrs(ctx, slog.LevelDebug, "GetBannedUsersFromRoom called")

	span := ru.telemetry.Record(ctx, "GetBannedUsersFromRoom",
		map[string]string{
			"roomId": roomId,
		})
	defer span.End()

	// Parse roomId and validate input data
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return nil, errors.Join(errdefs.ErrInvalidData, err)
	}

	// TODO: move to bingameutils
	if len(strings.TrimSpace(roomId)) == 0 {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: roomId is empty")
		return nil, errdefs.ErrInvalidData
	}

	// Retrieve and return the list of banned users
	users, err := ru.repository.GetBannedUsersFromRoom(ctx, roomId)
	if err != nil {
		return nil, errors.Join(errdefs.ErrDatabaseFailed, err)
	}

	return users, nil
}
