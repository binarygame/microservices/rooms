package usecase

import (
	"context"
	"errors"
	"log/slog"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
)

// Subscribe subscribes a client to receive events for a specific room by roomId.
// It creates a new client, adds it to the room's client list,
// and listens for context cancellation to remove the client.
func (ru *rooms) Subscribe(ctx context.Context, roomId string) (<-chan model.Event, error) {
	logger := ru.logger.WithGroup("subscribe").With("roomID", roomId)
	logger.LogAttrs(ctx, slog.LevelDebug, "Subscribe called")

	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return nil, errors.Join(errdefs.ErrInvalidData, err)
	}

	// Get or create the room based on the roomId
	room := ru.manager.GetOrCreateRoom(roomId)

	// Create a new client for the room to receive events
	client := bingameutils.NewRoomManagerClient[model.Event]()
	room.AddClient(client)

	// Handle context cancellation: when the context is done, remove the client and close the channel
	go func() {
		<-ctx.Done()
		room.RemoveClient(client.ClientID)
		close(client.Channel)
	}()

	return client.Channel, nil
}

// SubscribeToAll subscribes a client to receive events from all rooms.
// Similar to Subscribe, but it listens to a broadcast room
// that gets events from all rooms.
func (ru *rooms) SubscribeToAll(ctx context.Context) (<-chan model.Event, error) {
	logger := ru.logger.WithGroup("subscribe_to_all")
	logger.LogAttrs(ctx, slog.LevelDebug, "SubscribeToAll called")

	room := ru.manager.GetOrCreateBroadcastRoom()

	// Create a new client to receive broadcasted events
	client := bingameutils.NewRoomManagerClient[model.Event]()
	room.AddClient(client)

	// Handle context cancellation: when the context is done, remove the client and close the channel
	go func() {
		<-ctx.Done()
		room.RemoveClient(client.ClientID)
		close(client.Channel)
	}()

	return client.Channel, nil
}

// startBackgroundTasks subscribes to the event repository and broadcasts events to the appropriate rooms.
func (ru *rooms) startBackgroundTasks() {
	ru.repository.SubscribeToEvents(context.Background(), func(event model.Event) {
		ru.manager.Broadcast(event, event.GetRoomID())
	})
}
