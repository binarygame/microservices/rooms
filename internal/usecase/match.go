package usecase

import (
	"context"
	"errors"
	"log/slog"
	"time"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	guessesv1 "gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1"
	questionsv1 "gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1"
	qcrpcModel "gitlab.com/binarygame/microservices/questions/pkg/models"
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
)

// HandleUserFinishedAnswering handles the event when a user finishes answering in a room.
// It removes the user from the list of players yet to finish and checks if all players have finished answering.
// If all players have finished, it ends the match.
func (ru *rooms) HandleUserFinishedAnswering(ctx context.Context, roomId string, userID string) error {
	logger := ru.logger.With(slog.Group("input",
		"room_id", roomId,
		"user_id", userID,
	))

	logger.Debug("Received HandleUserFinishedAnswering request")

	span := ru.telemetry.Record(ctx, "HandleUserFinishedAnswering",
		map[string]string{
			"roomId": roomId,
			"userId": userID,
		})
	defer span.End()

	// Validate input data
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return errors.Join(errdefs.ErrInvalidData, err)
	}

	players, err := ru.repository.PlayersYetToFinish(ctx, roomId)
	if err != nil {
		if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
			logger.Error("HandleUserFinishedAnswering received request for room that does not exist", "error", err.Error())
			return err
		}
		logger.Error("Could not get players yet to finish", "error", err.Error())
		return errdefs.ErrDatabaseFailed
	}

	if _, ok := players[userID]; !ok {
		logger.Info("Received user finished answering request but user is already marked as finished")
		return errdefs.ErrUserAlreadyFinished
	}

	err = ru.repository.RemoveFromPlayersYetToFinish(ctx, roomId, userID)
	if err != nil {
		logger.Error("Could not remove players from yet to finish list in repository", "error", err.Error())
		return errdefs.ErrDatabaseFailed
	}

	if len(players) == 1 {
		logger.Debug("Last user marked as finished, ending match")
		// Match finished, everyone answered.
		err := ru.handleEndMatch(ctx, roomId, true)
		if err != nil {
			logger.Error("Could not end match", "error", err.Error())
			return errdefs.ErrInternalServer
		}
	}

	logger.Debug("HandleUserFinishedAnswering ran without errors")
	return nil
}

// StartMatch initiates the match for the given roomID. It sets the room status to starting,
// resets user scores, generates a question set, and publishes relevant events.
// It then spawns a background process to manage the countdown and match end.
func (ru *rooms) StartMatch(ctx context.Context, roomId string) error {
	logger := ru.logger.WithGroup("start_match").With(
		"roomId", roomId,
		"callerId", bingameutils.GetUserIdFromCtx(ctx),
	)
	logger.LogAttrs(ctx, slog.LevelDebug, "StartMatch called")

	span := ru.telemetry.Record(ctx, "StartMatch", map[string]string{"roomId": roomId})
	defer span.End()

	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return errors.Join(errdefs.ErrInvalidData, err)
	}

	callerId := bingameutils.GetUserIdFromCtx(ctx)
	roomObj, err := ru.repository.GetRoomById(ctx, roomId)
	if err != nil {
		return handleErrorForRoom(logger, ctx, err, errdefs.ErrDatabaseFailed)
	}

	if callerId != "" && callerId != roomObj.HostID {
		logger.LogAttrs(ctx, slog.LevelError, "Forbidden access: caller is not the host")
		return errdefs.ErrForbbidenAccess
	}

	err = ru.updateRoomToStarting(ctx, roomObj)
	if err != nil {
		return handleErrorForRoom(logger, ctx, err, errdefs.ErrDatabaseFailed)
	}

	err = ru.resetUserScores(ctx, roomId, roomObj.QuestionCount)
	if err != nil {
		return handleErrorForRoom(logger, ctx, err, errdefs.ErrInternalServer)
	}

	users, err := ru.repository.GetUsersInRoom(ctx, roomId)
	if err != nil {
		return handleErrorForRoom(logger, ctx, err, errdefs.ErrDatabaseFailed)
	}

	err = ru.repository.SetPlayersYetToFinish(ctx, roomId, users)
	if err != nil {
		return handleErrorForRoom(logger, ctx, err, errdefs.ErrDatabaseFailed)
	}

	questionSetId, err := ru.generateAndServeQuestionSet(ctx, roomObj.QuestionCount, roomObj.Level, roomObj.Shuffle)
	if err != nil {
		return handleErrorForRoom(logger, ctx, err, errdefs.ErrInternalServer)
	}

	err = ru.repository.SaveQuestionSet(ctx, roomId, questionSetId)
	if err != nil {
		return handleErrorForRoom(logger, ctx, err, errdefs.ErrDatabaseFailed)
	}

	rsEvent := model.NewRoomStartingEvent(roomId, "Room match is starting", questionSetId)
	ru.repository.PublishEvent(ctx, rsEvent)
	go ru.startMatchInBackground(ctx, roomId, roomObj, rsEvent.GetTimestamp())
	return nil
}

// handleErrorForRoom handles errors related to room operations. If the error
// indicates that the room does not exist, it logs and returns the original error.
// Otherwise, it logs the error and returns a fallback error.
func handleErrorForRoom(logger *slog.Logger, ctx context.Context, err error, fallbackErr error) error {
	if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
		logger.LogAttrs(ctx, slog.LevelInfo, "Room with provided id does not exist")
		return err
	}
	logger.LogAttrs(ctx, slog.LevelError, "Failed operation in repository", slog.String("error", err.Error()))
	return fallbackErr
}

// updateRoomToStarting sets the status of the room object to 'Starting' and
// updates it in the repository.
func (ru *rooms) updateRoomToStarting(ctx context.Context, roomObj *model.Room) error {
	roomObj.Status = model.Starting
	return ru.repository.UpdateRoomStatus(ctx, roomObj.ID, model.Starting)
}

// resetUserScores resets the scores of all users in the room using the guesses client.
func (ru *rooms) resetUserScores(ctx context.Context, roomID string, questionCount uint32) error {
	_, err := ru.guessesClient.ResetScores(
		ctx,
		connect.NewRequest(&guessesv1.ResetScoresRequest{
			RoomId:        roomID,
			QuestionCount: questionCount,
		}),
	)
	return err
}

// generateAndServeQuestionSet generates a question set for the room using the
// questions client and returns the question set ID.
func (ru *rooms) generateAndServeQuestionSet(ctx context.Context, questionCount uint32, level questionsModel.QuestionSetLevel, shuffle bool) (string, error) {

	var protoLevel questionsv1.DifficultyLevel
	for key, val := range qcrpcModel.ProtoLevelMap {
		if val == level {
			protoLevel = key
		}
	}

	qsetRes, err := ru.questionsClient.GenerateQuestionSet(
		ctx,
		connect.NewRequest(&questionsv1.GenerateQuestionSetRequest{
			QuestionCount: int32(questionCount),
			Difficulty:    protoLevel,
			Shuffle:       shuffle,
		}),
	)
	if err != nil {
		return "", err
	}

	questionSet := qsetRes.Msg.GetQuestionSet()
	if questionSet == nil {
		return "", errors.New("failed to generate question set")
	}
	return questionSet.Id, nil
}

// startMatchInBackground manages the background process that handles the countdown
// to the start of the match and the subsequent match end subscription.
func (ru *rooms) startMatchInBackground(ctx context.Context, roomID string, roomObj *model.Room, startTimestamp time.Time) {
	// Spawn background task to effectively start match
	ctx = context.Background()

	// TODO: make the countdown timer a room config

	// Start countdown sleep
	endMatchCh := ru.startCountdownForMatch(ctx, roomObj, startTimestamp)
	ru.handleEndMatchSubscription(ctx, roomID, endMatchCh)
}

// startCountdownForMatch starts the countdown for the match, updates the room status
// to 'Playing', and sets a timer for the match end. It returns a channel that signals
// when the match should end.
func (ru *rooms) startCountdownForMatch(ctx context.Context, roomObj *model.Room, startTimestamp time.Time) <-chan time.Time {
	time.Sleep(constants.DefaultStartsIn - time.Since(startTimestamp))

	err := ru.repository.UpdateRoomStatus(ctx, roomObj.ID, model.Playing)
	if err != nil {
		ru.handleBackendErrorDuringMatchStart(ctx, roomObj.ID, err, model.ErrorStarting)
		return nil
	}

	err = ru.repository.UpdateRoomLastStartTime(ctx, roomObj.ID, &startTimestamp)
	if err != nil {
		ru.handleBackendErrorDuringMatchStart(ctx, roomObj.ID, err, model.ErrorStarting)
		return nil
	}

	startEvent := model.NewGenericEventWithTimestamp(startTimestamp, model.RoomPlaying, roomObj.ID, "Room match started")
	ru.repository.PublishEvent(ctx, startEvent)
	ru.telemetry.IncrementCounterUpDown(ctx, constants.RoomsPlayingKey, 1)

	// Set the timer for match end
	endTime := startEvent.GetTimestamp().Add(roomObj.PlayingDuration)
	endTimer := time.After(time.Until(endTime))
	return endTimer
}

// handleEndMatchSubscription manages the subscription to the end match event.
// It listens for match end notifications and handles match end accordingly.
func (ru *rooms) handleEndMatchSubscription(ctx context.Context, roomID string, endTimer <-chan time.Time) {
	endMatchCh, err := ru.repository.SubscribeToEndMatch(ctx, roomID)
	if err != nil {
		ru.logger.Error("Failed to subscribe to end match channel", "error", err.Error())
		return
	}

	for {
		select {
		case endMatchMsg := <-endMatchCh:
			if endMatchMsg.RoomID == roomID {
				ru.finalizeMatchEnd(ctx, roomID, false)
				return
			}
		case <-endTimer:
			ru.finalizeMatchEnd(ctx, roomID, true)
			return
		}
	}
}

// finalizeMatchEnd finalizes the ending of the match by calling handleEndMatch and
// publishing relevant events if an error occurs.
func (ru *rooms) finalizeMatchEnd(ctx context.Context, roomID string, publish bool) {
	err := ru.handleEndMatch(ctx, roomID, publish)
	if err != nil {
		ru.logger.Error("Could not end match", "error", err.Error())
		errEvent := model.NewGenericEvent(model.ErrorEnding, roomID, err.Error())
		err := ru.repository.PublishEvent(ctx, errEvent)
		if err != nil {
			ru.logger.Error("Could not send ErrorEnding event", "event_id", errEvent.GetID(), "room_id", errEvent.GetRoomID(), "event_description", errEvent.GetDescription())
		}
	}
}

// handleBackendErrorDuringMatchStart handles errors that occur during the match starting
// process by logging the error and publishing an error event.
func (ru *rooms) handleBackendErrorDuringMatchStart(ctx context.Context, roomID string, err error, eventType model.EventType) {
	ru.logger.LogAttrs(ctx, slog.LevelError, "Failed to update room status", slog.String("error", err.Error()))
	errorEvent := model.NewGenericEvent(eventType, roomID, "An internal server error occurred\n"+err.Error())
	err = ru.repository.PublishEvent(ctx, errorEvent)
	if err != nil {
		ru.logger.Error("Could not send error event", "error", err.Error())
	}
}

// handleEndMatch removes the end match timer, publishes an end match message,
// updates the room status to 'Ending', and publishes the end match event.
func (ru *rooms) handleEndMatch(ctx context.Context, roomID string, publish bool) error {
	errFailedEndMatchPublish := errors.New("failed to publish end match to repository")
	errFailedUpdateRoomStatusToEnding := errors.New("failed to update room status to ending")
	errFailedEndMatchEventPublish := errors.New("failed to publish end match event")

	err := ru.repository.UpdateRoomStatus(ctx, roomID, model.Ending)
	if err != nil {
		return errors.Join(err, errFailedUpdateRoomStatusToEnding)
	}

	if publish {
		// Publish an end match message to all instances (backend communication)
		err := ru.repository.PublishEndMatch(ctx, roomID)
		if err != nil {
			return errors.Join(err, errFailedEndMatchPublish)
		}
		// Send out room ending event to clients (user communication)
		err = ru.repository.PublishEvent(ctx, model.NewGenericEvent(model.RoomEnding, roomID, "Match ended"))
		if err != nil {
			return errors.Join(err, errFailedEndMatchEventPublish)
		}
	}

	return nil
}
