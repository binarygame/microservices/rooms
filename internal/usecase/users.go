package usecase

import (
	"context"
	"errors"
	"log/slog"
	"math/rand/v2"
	"strings"
	"sync"
	"time"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
)

func (ru *rooms) GetUsers(ctx context.Context, roomId string) ([]string, error) {
	// Logger setup for GetUsers function with room and caller details
	logger := ru.logger.WithGroup("get_users").With(
		"roomId", roomId,
		"callerId", bingameutils.GetUserIdFromCtx(ctx),
	)

	// Log the GetUsers function call
	logger.LogAttrs(ctx, slog.LevelDebug, "GetUsers called")

	// Start telemetry span for performance tracking
	span := ru.telemetry.Record(ctx, "GetUsers",
		map[string]string{
			"roomId": roomId,
		})
	defer span.End()

	// Parse the roomId and handle invalid data
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return nil, errors.Join(errdefs.ErrInvalidData, err)
	}

	// Fetch users in the room from the repository
	users, err := ru.repository.GetUsersInRoom(ctx, roomId)
	if err != nil {
		// Log and handle case when the room doesn't exist
		if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
			logger.LogAttrs(ctx, slog.LevelInfo, "Room with provided id does not exist")
			return nil, err
		}
		// Log and return database failure
		logger.LogAttrs(ctx, slog.LevelError, "Failed to get room by ID", slog.String("error", err.Error()))
		return nil, errdefs.ErrDatabaseFailed
	}

	// Return the list of users in the room
	return users, nil
}

// UserDisconnected is a callback function that is called when an user
// from a room does not ping our servers (the RegisterUser function)
// inside a timeframe defined in constants.JoinRoomPingInterval
//
// This makes the user leave the room, sending an event when this happens
func (ru *rooms) UserDisconnected(roomId, userId string) {
	ctx := context.Background()

	logger := ru.logger.WithGroup("user_disconnected").With(
		"roomId", roomId,
		"userId", userId,
	)

	// Log function call
	logger.Debug("User disconnected callback called")

	span := ru.telemetry.Record(ctx, "UserDisconnected",
		map[string]string{
			"userId": userId,
			"roomId": roomId,
		})
	defer span.End()

	// Parse roomId and validate inputs
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid room id in autodisconnect map", slog.Any("error", err))
		return
	}

	userInRoom, err := ru.repository.IsUserInRoom(ctx, roomId, userId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Could not check if user is in room", slog.Any("error", err))
		return
	}

	if !userInRoom {
		logger.LogAttrs(ctx, slog.LevelDebug, "Autodisconnect ran on user that already left")
		return
	}

	// Fetch room details
	roomObj, err := ru.repository.GetRoomById(ctx, roomId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to get room by ID", slog.Any("error", err))
		return
	}

	// Remove user from the room
	err = ru.repository.RemoveUserFromRoom(ctx, userId, roomId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to remove user from room", slog.String("error", err.Error()))
		return
	}

	// Publish user removed event
	ru.repository.PublishEvent(ctx, model.NewUserEvent(model.UserLostConnection, roomId, userId, "User lost connection and was removed automatically"))

	if userId == roomObj.HostID {
		users, err := ru.repository.GetUsersInRoom(ctx, roomId)
		if err != nil && !errors.Is(err, errdefs.ErrRoomDoesNotExist) {
			logger.LogAttrs(ctx, slog.LevelError, "Could not get list of users in room", slog.Any("error", err))
			return
		}

		// Room has no one left, cleanup
		if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
			// TODO: implement cleanup
			return
		}

		// Choose new room host randomly
		newHost := users[rand.IntN(len(users))]

		err = ru.repository.UpdateRoomHostId(ctx, roomId, newHost)
		if err != nil {
			logger.LogAttrs(ctx, slog.LevelError, "Could not change room host id", slog.Any("error", err), slog.String("newHost", newHost))
			return
		}

		ru.repository.PublishEvent(ctx, model.NewUserEvent(model.UserHostChanged, roomId, newHost, "Host changed to a random one due to previous host losing connection"))
	}
}

// RegisterUser handles user registration to a room by validating inputs,
// ensuring the user isn't banned or the host, checking room capacity,
// and adding the user if all conditions are met. It also manages telemetry
// and publishes user events.
//
// It has a system to automatically disconnect a user if they don't call this
// function in the interval defined in constants.JoinRoomPingInterval
func (ru *rooms) RegisterUser(ctx context.Context, userId, roomId string) (expiresAt *time.Time, wasInRoomBefore bool, err error) {
	// Logger setup for RegisterUser function with user, room, and caller details
	logger := ru.logger.WithGroup("register_user").With(
		"userId", userId,
		"roomId", roomId,
		"callerId", bingameutils.GetUserIdFromCtx(ctx),
	)

	// Log the RegisterUser function call
	logger.LogAttrs(ctx, slog.LevelDebug, "RegisterUser called")

	// Start telemetry span for performance tracking
	span := ru.telemetry.Record(ctx, "RegisterUser",
		map[string]string{
			"userId": userId,
			"roomId": roomId,
		})
	defer span.End()

	// Parse roomId and validate inputs
	roomId, err = bingameutils.ParseRoomID(roomId)
	if err != nil {
		return nil, false, errors.Join(errdefs.ErrInvalidData, err)
	}

	// Validate callerId and userId match
	callerId := bingameutils.GetUserIdFromCtx(ctx)
	if (callerId != "" && callerId != userId) || !bingameutils.IsValidUUID(userId) {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: callerId mismatch or invalid userId")
		return nil, false, errdefs.ErrInvalidData
	}

	userInRoom, err := ru.repository.IsUserInRoom(ctx, roomId, userId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Could not check if user is already in the room", slog.Any("error", err))
		return nil, false, errdefs.ErrDatabaseFailed
	}

	// Skip if the user is already in the room
	if !userInRoom {
		// Check if user is banned from the room
		banned, err := ru.repository.IsUserBannedFromRoom(ctx, userId, roomId)
		if err != nil {
			logger.LogAttrs(ctx, slog.LevelError, "Could not check if user is banned from room", slog.Any("error", err))
			return nil, false, errors.Join(errdefs.ErrDatabaseFailed, err)
		}
		logger.LogAttrs(ctx, slog.LevelDebug, "Got user banned status", slog.Bool("banned", banned))

		// Return error if user is banned
		if banned {
			return nil, false, errdefs.ErrUserBannedFromRoom
		}

		// Fetch room details
		roomObj, err := ru.repository.GetRoomById(ctx, roomId)
		if err != nil {
			if errors.Is(err, errdefs.ErrRoomDoesNotExist) {
				logger.LogAttrs(ctx, slog.LevelDebug, "Tried to join room that doesn't exist", slog.String("error", err.Error()))
				return nil, false, err
			}
			logger.LogAttrs(ctx, slog.LevelError, "Failed to get room by ID", slog.String("error", err.Error()))
			return nil, false, errdefs.ErrDatabaseFailed
		}

		// Ensure user is not the host
		if userId == roomObj.HostID {
			logger.LogAttrs(ctx, slog.LevelError, "Invalid data: user cannot be the host")
			return nil, false, errdefs.ErrInvalidData
		}

		// Get current player count and check if room is full
		playersCount, err := ru.repository.GetRoomPlayersCount(ctx, roomId)
		if err != nil {
			logger.LogAttrs(ctx, slog.LevelError, "Failed to get room players count", slog.String("error", err.Error()))
			return nil, false, errdefs.ErrDatabaseFailed
		}
		if playersCount == roomObj.MaxPlayers {
			logger.LogAttrs(ctx, slog.LevelError, "Room is full")
			return nil, false, errdefs.ErrRoomIsFull
		}
	}

	// Add user to the room
	err = ru.repository.InsertUserInRoom(ctx, userId, roomId, constants.JoinRoomPingInterval*2) // Multiplied by two to prevent any weird edge cases
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to insert user into room", slog.String("error", err.Error()))
		return nil, false, errdefs.ErrBindUserRoomFailed
	}

	// Create/Update auto disconnect timer
	expiresAt = ru.userDisconnectTimers.Set(roomId, userId)

	// Increment telemetry counter for user registration or ping
	if userInRoom {
		ru.telemetry.IncrementCounter(ctx, constants.UsersRegisteredPingKey, 1)
	} else {
		ru.telemetry.IncrementCounter(ctx, constants.UsersRegisteredKey, 1)
	}

	// Publish user joined event if user wasn't in the room
	if !userInRoom {
		ru.repository.PublishEvent(ctx, model.NewUserEvent(model.UserJoined, roomId, userId, "User joined room"))
	}

	return expiresAt, userInRoom, nil
}

func (ru *rooms) RemoveUser(ctx context.Context, userId, roomId string) error {
	// Logger setup for RemoveUser function with user, room, and caller details
	logger := ru.logger.WithGroup("remove_user").With(
		"userId", userId,
		"roomId", roomId,
		"callerId", bingameutils.GetUserIdFromCtx(ctx),
	)

	// Log the RemoveUser function call
	logger.LogAttrs(ctx, slog.LevelDebug, "RemoveUser called")

	// Start telemetry span for performance tracking
	span := ru.telemetry.Record(ctx, "RemoveUser",
		map[string]string{
			"userId": userId,
			"roomId": roomId,
		})
	defer span.End()

	// Parse roomId and validate inputs
	roomId, err := bingameutils.ParseRoomID(roomId)
	if err != nil {
		return errors.Join(errdefs.ErrInvalidData, err)
	}

	// Validate input values
	callerId := bingameutils.GetUserIdFromCtx(ctx)
	if len(strings.TrimSpace(userId)) == 0 || len(strings.TrimSpace(roomId)) == 0 {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: userId or roomId is empty")
		return errdefs.ErrInvalidData
	}
	if (callerId != "" && callerId != userId) || !bingameutils.IsValidUUID(userId) {
		logger.LogAttrs(ctx, slog.LevelError, "Invalid data: callerId mismatch or invalid userId")
		return errdefs.ErrInvalidData
	}

	// Fetch room details
	roomObj, err := ru.repository.GetRoomById(ctx, roomId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to get room by ID", slog.String("error", err.Error()))
		return errors.Join(errdefs.ErrDatabaseFailed, err)
	}

	// Prevent host from leaving, TODO: handle host room deletion
	if userId == roomObj.HostID {
		return errdefs.ErrInvalidData
	}

	// Remove user from the room
	err = ru.repository.RemoveUserFromRoom(ctx, userId, roomId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Failed to remove user from room", slog.String("error", err.Error()))
		return errors.Join(errdefs.ErrDatabaseFailed, err)
	}

	// Publish user removed event
	ru.repository.PublishEvent(ctx, model.NewUserEvent(model.UserLeft, roomId, userId, "User removed from room"))
	return nil
}

// Data structure to enable users to automatically
// disconnect based on a background timer.
type roomUserMap struct {
	// map[roomId][userId]
	data       map[string]map[string]*time.Timer
	callback   func(roomId, userId string)
	expiration time.Duration
	mu         sync.Mutex
}

func newRoomUserMap(expiration time.Duration, callback func(roomID, userID string)) *roomUserMap {
	return &roomUserMap{
		data:       make(map[string]map[string]*time.Timer),
		expiration: expiration,
		callback:   callback,
	}
}

// Set adds or updates a roomId+userId combination and resets its timer.
func (r *roomUserMap) Set(roomId, userId string) *time.Time {
	r.mu.Lock()
	defer r.mu.Unlock()

	if _, exists := r.data[roomId]; !exists {
		r.data[roomId] = make(map[string]*time.Timer)
	}

	if timer, exists := r.data[roomId][userId]; exists {
		timer.Stop()
	}

	expiresAt := time.Now().Add(r.expiration)
	r.data[roomId][userId] = time.AfterFunc(r.expiration, func() {
		r.mu.Lock()
		delete(r.data[roomId], userId)
		if len(r.data[roomId]) == 0 {
			delete(r.data, roomId)
		}
		r.mu.Unlock()
		r.callback(roomId, userId)
	})

	return &expiresAt
}
