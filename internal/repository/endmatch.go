package repository

import (
	"context"

	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
)

type EndMatchMessage struct {
	RoomID string
}

func (r *roomsRepository) PublishEndMatch(ctx context.Context, roomID string) error {
	key := buildEndMatchChannelKey(roomID)
	return r.db.Publish(ctx, key, roomID).Err()
}

func (r *roomsRepository) SubscribeToEndMatch(ctx context.Context, roomID string) (<-chan EndMatchMessage, error) {
	key := buildEndMatchChannelKey(roomID)
	pubsub := r.db.Subscribe(ctx, key)
	msgCh := make(chan EndMatchMessage)

	go func() {
		defer pubsub.Close()
		defer close(msgCh)

		ch := pubsub.Channel()
		for {
			select {
			case msg := <-ch:
				endMatchMsg := EndMatchMessage{RoomID: msg.Payload}
				msgCh <- endMatchMsg
			case <-ctx.Done():
				return
			}
		}
	}()

	return msgCh, nil
}

func (r *roomsRepository) SetPlayersYetToFinish(ctx context.Context, roomID string, players []string) error {
	key := buildUsersFinishedGuessingSetKey(roomID)
	pipe := r.db.Pipeline()
	delcmd := pipe.Del(ctx, key)
	addcmd := pipe.SAdd(ctx, key, players)

	_, err := pipe.Exec(ctx)
	if err != nil {
		return err
	}

	if _, err := delcmd.Result(); err != nil {
		return err
	}
	if _, err := addcmd.Result(); err != nil {
		return err
	}

	return nil
}

func (r *roomsRepository) PlayersYetToFinish(ctx context.Context, roomID string) (map[string]struct{}, error) {
	key := buildUsersFinishedGuessingSetKey(roomID)
	result, err := r.db.SMembersMap(ctx, key).Result()
	if err != nil {
		return nil, err
	}
	if result == nil {
		return nil, errdefs.ErrRoomDoesNotExist
	}
	return result, nil
}

func (r *roomsRepository) RemoveFromPlayersYetToFinish(ctx context.Context, roomID, playerId string) error {
	key := buildUsersFinishedGuessingSetKey(roomID)
	_, err := r.db.SRem(ctx, key, playerId).Result()
	if err != nil {
		return err
	}
	return nil
}
