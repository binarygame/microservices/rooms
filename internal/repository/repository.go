package repository

import (
	"context"
	"errors"
	"math"
	"time"

	"github.com/redis/go-redis/extra/redisotel/v9"
	"github.com/redis/go-redis/v9"
	qmodel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
)

type RoomsRepository interface {
	/*
	   events.go functions
	*/
	PublishEvent(ctx context.Context, event model.Event) error
	SubscribeToEvents(ctx context.Context, eventHandler func(event model.Event)) error

	/*
	   rooms.go functions
	*/
	InsertRoom(ctx context.Context, room *model.Room) error
	UpdateRoom(ctx context.Context, id string, name string, hostId string, maxPlayers uint32, questionCount uint32, status model.RoomStatus, lastStarted *time.Time, level qmodel.QuestionSetLevel, shuffle bool) error
	UpdateRoomHostId(ctx context.Context, id string, hostId string) error
	UpdateRoomStatus(ctx context.Context, id string, status model.RoomStatus) error
	UpdateRoomLastStartTime(ctx context.Context, id string, lastStartedTime *time.Time) error
	UpdateRoomFinishedAnswering(ctx context.Context, id string, count uint32) error
	GetRoomById(ctx context.Context, id string) (*model.Room, error)
	CheckIfRoomExists(ctx context.Context, id string) (bool, error)

	/*
	   users.go functions
	*/
	GetUsersInRoom(ctx context.Context, roomId string) ([]string, error)
	InsertUserInRoom(ctx context.Context, userId string, roomId string, expiresIn time.Duration) error
	RemoveUserFromRoom(ctx context.Context, userId string, roomId string) error
	GetRoomPlayersCount(ctx context.Context, id string) (uint32, error)
	IsUserInRoom(ctx context.Context, roomID string, userID string) (bool, error)

	/*
	   questions.go functions
	*/
	GetQuestionCount(ctx context.Context, roomId string) (int, error)
	ClearQuestionSet(ctx context.Context, roomId string) error
	SaveQuestionSet(ctx context.Context, roomId string, questionSet string) error
	GetQuestionSet(ctx context.Context, roomId string) (string, error)

	/*
	   endmatch.go functions
	*/
	PublishEndMatch(ctx context.Context, roomID string) error
	SubscribeToEndMatch(ctx context.Context, roomID string) (<-chan EndMatchMessage, error)
	SetPlayersYetToFinish(ctx context.Context, roomID string, players []string) error
	PlayersYetToFinish(ctx context.Context, roomID string) (map[string]struct{}, error)
	RemoveFromPlayersYetToFinish(ctx context.Context, roomID, playerId string) error

	/*
	   ban.go functions
	*/
	GetBannedUsersFromRoom(ctx context.Context, roomId string) ([]string, error)
	BanUserFromRoom(ctx context.Context, userId string, roomId string) error
	UnbanUserFromRoom(ctx context.Context, userId string, roomId string) error
	IsUserBannedFromRoom(ctx context.Context, userID, roomID string) (bool, error)
}

type roomsRepository struct {
	db *redis.Client
}

func New(address string) *roomsRepository {
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// Enable tracing instrumentation.
	if err := redisotel.InstrumentTracing(client); err != nil {
		panic(err)
	}

	// Enable metrics instrumentation.
	if err := redisotel.InstrumentMetrics(client); err != nil {
		panic(err)
	}

	return &roomsRepository{
		db: client,
	}
}

func safeCastInt64ToUint32(val int64) (uint32, error) {
	if val < 0 || val > math.MaxUint32 {
		return 0, errors.New("int64 value is out of uint32 range")
	}
	return uint32(val), nil
}

func boolToInt8(v bool) int8 {
	if v == true {
		return 1
	}
	return 0
}
