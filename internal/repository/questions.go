package repository

import (
	"context"
	"strconv"

	"github.com/redis/go-redis/v9"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
)

func (ru *roomsRepository) GetQuestionCount(ctx context.Context, roomId string) (int, error) {
	// Build the key using the roomId
	key := buildRoomKey(roomId)

	// Use HGet to get the question count from Valkey
	questionCountStr, err := ru.db.HGet(ctx, key, "questioncount").Result()
	if err != nil {
		if err == redis.Nil {
			// Handle the case where the key does not exist
			return 0, errdefs.ErrRoomDoesNotExist
		}
		return 0, err
	}

	// Convert the question count from string to integer
	questionCount, err := strconv.Atoi(questionCountStr)
	if err != nil {
		return 0, errdefs.ErrRoomContainsInvalidData
	}

	return questionCount, nil
}

func (ru *roomsRepository) ClearQuestionSet(ctx context.Context, roomId string) error {
	// Build the key using the roomId
	key := buildRoomKey(roomId)

	// Use HSet to clear the question set id from the room
	_, err := ru.db.HSet(ctx, key, "questionset", "").Result()
	if err != nil {
		if err == redis.Nil {
			return errdefs.ErrRoomDoesNotExist
		}
		return err
	}
	return nil
}

func (ru *roomsRepository) SaveQuestionSet(ctx context.Context, roomId string, questionSet string) error {
	// Build the key using the roomId
	key := buildRoomKey(roomId)

	// Use HSet to clear the question set id from the room
	_, err := ru.db.HSet(ctx, key, "questionset", questionSet).Result()
	if err != nil {
		if err == redis.Nil {
			return errdefs.ErrRoomDoesNotExist
		}
		return err
	}
	return nil
}

func (ru *roomsRepository) GetQuestionSet(ctx context.Context, roomId string) (string, error) {
	// Build the key using the roomId
	key := buildRoomKey(roomId)

	// Use HGet to get the question count from Valkey
	questionSet, err := ru.db.HGet(ctx, key, "questionset").Result()
	if err != nil {
		if err == redis.Nil {
			// Handle the case where the key does not exist
			return "", errdefs.ErrRoomDoesNotExist
		}
		return "", err
	}

	return questionSet, nil
}
