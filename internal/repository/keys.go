package repository

const (
	roomsRedisKey             = "rooms"
	roomsBannedUsersKey       = "bannedusers"
	usersRedisKey             = "users"
	questionsRedisKey         = "questions"
	eventsRedisKey            = "events:rooms"
	endMatchChannelKey        = "endmatchchannel"
	usersFinishedAnsweringKey = "finishedanswering"
)

func buildRoomKey(id string) string {
	return roomsRedisKey + ":" + id
}

func buildUserRoomSetKey(roomId string) string {
	return roomsRedisKey + ":" + roomId + ":" + usersRedisKey
}

func buildEndMatchChannelKey(roomID string) string {
	return roomsRedisKey + ":" + roomID + ":" + endMatchChannelKey
}

func buildUsersFinishedGuessingSetKey(roomID string) string {
	return roomsRedisKey + ":" + roomID + ":" + usersFinishedAnsweringKey
}

func buildRoomBannedUsersKey(roomID string) string {
	return roomsRedisKey + ":" + roomID + ":" + roomsBannedUsersKey
}
