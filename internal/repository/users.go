package repository

import (
	"context"
	"time"

	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
)

func (g *roomsRepository) GetUsersInRoom(ctx context.Context, roomId string) ([]string, error) {
	sKey := buildUserRoomSetKey(roomId)

	r, err := g.db.SMembers(ctx, sKey).Result()
	if err != nil {
		return nil, err
	}

	// Check if the set is empty
	if len(r) == 0 {
		return nil, errdefs.ErrRoomDoesNotExist
	}

	return r, nil
}

func (g *roomsRepository) InsertUserInRoom(ctx context.Context, userId string, roomId string, expiresIn time.Duration) error {
	sKey := buildUserRoomSetKey(roomId)

	_, err := g.db.SAdd(ctx, sKey, userId).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, sKey, expiresIn).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) RemoveUserFromRoom(ctx context.Context, userId string, roomId string) error {
	sKey := buildUserRoomSetKey(roomId)

	_, err := g.db.SRem(ctx, sKey, userId).Result()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) GetRoomPlayersCount(ctx context.Context, roomID string) (uint32, error) {
	key := buildUserRoomSetKey(roomID)

	result := g.db.SCard(ctx, key)
	if err := result.Err(); err != nil {
		return 0, err
	}

	return safeCastInt64ToUint32(result.Val())
}

func (g *roomsRepository) IsUserInRoom(ctx context.Context, roomID string, userID string) (bool, error) {
	key := buildUserRoomSetKey(roomID)

	result := g.db.SIsMember(ctx, key, userID)
	if err := result.Err(); err != nil {
		return false, err
	}

	return result.Val(), nil
}
