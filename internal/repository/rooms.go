package repository

import (
	"context"
	"errors"
	"strconv"
	"time"

	qmodel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
	"gitlab.com/binarygame/microservices/rooms/pkg/errdefs"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
)

// Implementing the methods related to Room operations.

func formatLastStarted(t *time.Time) string {
	var lastStarted string
	if t == nil {
		lastStarted = ""
	} else {
		lastStarted = strconv.FormatInt(t.UnixMilli(), 10)
	}

	return lastStarted
}

func parseLastStarted(s string) (*time.Time, error) {
	if s == "" {
		return nil, nil
	}

	u, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return nil, err
	}

	t := time.UnixMilli(u)
	return &t, nil
}

func (g *roomsRepository) InsertRoom(ctx context.Context, room *model.Room) error {
	key := buildRoomKey(room.ID)

	_, err := g.db.HSet(ctx, key,
		"name", room.Name,
		"hostid", room.HostID,
		"maxplayers", room.MaxPlayers,
		"questioncount", room.QuestionCount,
		"status", int(room.Status),
		"laststarted", formatLastStarted(room.LastTimeStarted),
		"questionset", "", // start empty, no questions generated
		"finishedanswering", 0,
		"playingduration", room.PlayingDuration.String(),
		"level", int(room.Level),
		"shuffle", boolToInt8(room.Shuffle),
	).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, key, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) UpdateRoom(
	ctx context.Context,
	id string,
	name string,
	hostId string,
	maxPlayers uint32,
	questionCount uint32,
	status model.RoomStatus,
	lastStarted *time.Time,
	level qmodel.QuestionSetLevel,
	shuffle bool) error {
	key := buildRoomKey(id)

	_, err := g.db.HSet(ctx, key,
		"name", name,
		"hostid", hostId,
		"maxplayers", maxPlayers,
		"questioncount", questionCount,
		"status", int(status),
		"laststarted", formatLastStarted(lastStarted),
		"level", int(level),
		"shuffle", boolToInt8(shuffle),
	).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, key, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) UpdateRoomHostId(ctx context.Context, id string, hostId string) error {
	key := buildRoomKey(id)
	var err error

	_, err = g.db.HSet(ctx, key, "hostid", hostId).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, key, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) UpdateRoomFinishedAnswering(ctx context.Context, id string, count uint32) error {
	key := buildRoomKey(id)
	var err error

	_, err = g.db.HSet(ctx, key, "finishedanswering", count).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, key, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) UpdateRoomStatus(ctx context.Context, id string, status model.RoomStatus) error {
	key := buildRoomKey(id)
	var err error

	_, err = g.db.HSet(ctx, key, "status", int(status)).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, key, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) UpdateRoomLastStartTime(ctx context.Context, id string, lastStartedTime *time.Time) error {
	key := buildRoomKey(id)
	var err error

	_, err = g.db.HSet(ctx, key, "laststarted", formatLastStarted(lastStartedTime)).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, key, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) GetRoomById(ctx context.Context, id string) (*model.Room, error) {
	key := buildRoomKey(id)

	// Fetch the data from Redis
	result, err := g.db.HGetAll(ctx, key).Result()
	if err != nil {
		return nil, err
	}

	// Check if the room exists
	if len(result) == 0 {
		return nil, errdefs.ErrRoomDoesNotExist
	}

	// Initialize an empty room instance
	var room model.Room

	// Iterate over the result and manually scan the values into the struct
	for field, value := range result {
		switch field {
		case "name":
			room.Name = value
		case "hostid":
			room.HostID = value
		case "maxplayers":
			maxPlayers, err := strconv.ParseUint(value, 10, 32)
			if err != nil {
				return nil, errors.Join(err, errors.New("maxplayers value stored in database is not an uint32"))
			}
			room.MaxPlayers = uint32(maxPlayers)
		case "questioncount":
			questionCount, err := strconv.ParseUint(value, 10, 32)
			if err != nil {
				return nil, errors.Join(err, errors.New("questioncount value stored in database is not an uint32"))
			}
			room.QuestionCount = uint32(questionCount)
		case "status":
			v, err := strconv.Atoi(value)
			if err != nil {
				return nil, errors.Join(err, errors.New("status value stored in database is not an int32"))
			}
			room.Status = model.RoomStatus(v)
		case "laststarted":
			v, err := parseLastStarted(value)
			if err != nil {
				return nil, errors.Join(err, errors.New("laststarted value could not be parsed"))
			}
			room.LastTimeStarted = v
		case "questionset":
			room.QuestionSetID = value
		case "finishedanswering":
			finishedAnswering, err := strconv.ParseUint(value, 10, 32)
			if err != nil {
				return nil, errors.Join(err, errors.New("finishedanswering value stored in database is not an uint32"))
			}
			room.FinishedAnsweringCount = uint32(finishedAnswering)
		case "playingduration":
			d, err := time.ParseDuration(value)
			if err != nil {
				return nil, errors.Join(err, errors.New("playingduration value stored in database is not a golang duration"))
			}
			room.PlayingDuration = d
		case "level":
			v, err := strconv.Atoi(value)
			if err != nil {
				return nil, errors.Join(err, errors.New("level value stored in database is not an int32"))
			}
			room.Level = qmodel.QuestionSetLevel(v)
		case "shuffle":
			v, err := strconv.Atoi(value)
			if err != nil {
				return nil, errors.Join(err, errors.New("shuffle value stored in database is not an int32"))
			}
			room.Shuffle = (v == 1)
		}
	}
	room.ID = id

	return &room, nil
}

// Returns true if a room with the provided ID exists.
func (g *roomsRepository) CheckIfRoomExists(ctx context.Context, id string) (bool, error) {
	key := buildRoomKey(id)
	result, err := g.db.Exists(ctx, key).Result()
	if err != nil {
		return false, err
	}
	if result == 1 {
		return true, nil
	} else {
		return false, nil
	}
}
