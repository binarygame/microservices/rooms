package repository

import (
	"context"

	"gitlab.com/binarygame/microservices/rooms/pkg/constants"
)

func (g *roomsRepository) GetBannedUsersFromRoom(ctx context.Context, roomId string) ([]string, error) {
	sKey := buildRoomBannedUsersKey(roomId)

	r, err := g.db.SMembers(ctx, sKey).Result()
	if err != nil {
		return nil, err
	}

	return r, nil
}

func (g *roomsRepository) BanUserFromRoom(ctx context.Context, userId string, roomId string) error {
	sKey := buildRoomBannedUsersKey(roomId)

	_, err := g.db.SAdd(ctx, sKey, userId).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, sKey, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) UnbanUserFromRoom(ctx context.Context, userId string, roomId string) error {
	sKey := buildRoomBannedUsersKey(roomId)

	_, err := g.db.SRem(ctx, sKey, userId).Result()
	if err != nil {
		return err
	}

	return nil
}

func (g *roomsRepository) IsUserBannedFromRoom(ctx context.Context, userID, roomID string) (bool, error) {
	sKey := buildRoomBannedUsersKey(roomID)

	result := g.db.SIsMember(ctx, sKey, userID)
	if err := result.Err(); err != nil {
		return false, err
	}

	return result.Val(), nil
}
